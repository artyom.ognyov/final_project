\babel@toc {russian}{}
\contentsline {section}{Введение}{5}{}%
\contentsline {section}{\numberline {1}Обоснование выбора темы}{6}{}%
\contentsline {section}{\numberline {2}Исследовательская часть}{8}{}%
\contentsline {subsection}{\numberline {2.1}Стилистические решения в традиционной анимации}{8}{}%
\contentsline {subsection}{\numberline {2.2}Возможности рисованной компьютерной анимации}{13}{}%
\contentsline {subsection}{\numberline {2.3}Плоские художественные стили}{16}{}%
\contentsline {subsection}{\numberline {2.4}Плоские стили в современной иллюстрации}{19}{}%
\contentsline {subsection}{\numberline {2.5}Цветовые схемы в современной иллюстрации}{24}{}%
\contentsline {subsection}{\numberline {2.6}Плоские стили в современной анимации}{26}{}%
\contentsline {section}{\numberline {3}Художественная часть}{32}{}%
\contentsline {subsection}{\numberline {3.1}Процесс разработки сценария фильма}{32}{}%
\contentsline {subsection}{\numberline {3.2}Выбор названия}{33}{}%
\contentsline {subsection}{\numberline {3.3}Разработка дизайна персонажей}{36}{}%
\contentsline {subsection}{\numberline {3.4}Разработка фоновых рисунков}{43}{}%
\contentsline {section}{\numberline {4}Техническая часть}{49}{}%
\contentsline {section}{Заключение}{54}{}%
\contentsline {section}{Библиографический список}{55}{}%
\contentsline {section}{Приложение А. Киносценарий}{56}{}%
\contentsline {section}{Приложение Б. Листы персонажей}{59}{}%
\contentsline {section}{Приложение В. Фоны и компоновочные кадры}{61}{}%
